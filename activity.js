// =====
// Quiz
// =====
// 1. What is the blueprint where objects are created from?
// Classes

// 2. What is the naming convention applied to classes?
// Capitalized

// 3. What keyword do we use to create objects from a class?
// new

// 4. What is the technical term for creating an object from a class?
// Instantiating

// 5. What class method dictates HOW objects will be created from that class?
// Constructor



// =====
// Function Coding
// =====
// 1. Define a grades property in our Student class that will accept an array of 4 numbers ranging from 0 to 100 to be its value. If the argument used does not satisfy all the conditions given, this property will be set to undefined instead.
class Student {
    constructor(name,email,grades){
        this.name = name;
        this.email = email;
        this.gradeAve = undefined;
        this.passed = undefined
        this.passedWithHonors = undefined
        if(Array.isArray(grades) && grades.length === 4){
            (grades.every(num => (num >= 0 && num <= 100)))? this.grades = grades : this.grades = undefined
        }
        else this.grades = undefined;
    }
    login(){
        console.log(`${this.email} has logged in`)
        return this
    }

    logout(){
        console.log(`${this.email} has logged out`)
        return this
    }

    listGrades(){
        console.log(`${this.name}'s quarterly grades are: ${this.grades}`)
        return this
    }
    computeAve() {
        this.gradeAve = (this.grades.reduce((a, b) => a + b) / this.grades.length)
        return this
    }
    willPass() {
        this.computeAve()
        this.passed = this.gradeAve >= 85
        return this
    }
    willPassWithHonors() {
        this.willPass()
        if(this.passed){
            if (this.gradeAve >= 90){
                this.passedWithHonors = true
            }else {
                this.passedWithHonors = false;
            }    
        } else this.passedWithHonors = undefined
        return this
    }
}

// 2. Instantiate all four students from the previous session from our Student class. Use the same values as before for their names, emails, and grades.

let StudentOne = new Student("John","john@mail.com",[89,84,78,88])
let StudentTwo = new Student("Joe","joe@mail.com",[78,82,79,85])
let StudentThree = new Student("Jane","jane@mail.com",[87,89,91,93])
let StudentFour = new Student("Jessie","jessie@mail.com",[91,89,92,93])

console.log(StudentOne)
console.log(StudentTwo)
console.log(StudentThree)
console.log(StudentFour)

// Quiz no. 2
// Quiz:
// // 1. Should class methods be included in the class constructor?
//     No
// // 2. Can class methods be separated by commas?
//     No
// // 3. Can we update an object’s properties via dot notation?
//     Yes
// // 4. What do you call the methods used to regulate access to an object’s properties?
//     Setter Getter
// // 5. What does a method need to return in order for it to be chainable?
//     this